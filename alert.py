import os

def alert(freq, duration):
	os.system('play -nq -t alsa synth {} sine {}'.format(duration, freq))

def start():
	alert(400, 1)

def end():
	alert(500, 2)

def switch():
	alert(300, 0.5)
	alert(300, 0.5)
