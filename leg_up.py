from time import sleep
from alert import start, switch, end
	
REPEAT_COUNT = 10
SECONDS = 10
REST_SECONDS = 5
UP_SECONDS = 2
DOWN_SECONDS = 5

def workout():
	print('Exercicio iniciado')

	for number in range(REPEAT_COUNT):
		print(number+1)

		start()
		sleep(UP_SECONDS)
		sleep(SECONDS)

		switch()
		sleep(DOWN_SECONDS)
		sleep(REST_SECONDS)

	end()

if __name__ == '__main__':
	workout()
