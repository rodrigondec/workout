from time import sleep
from alert import start, switch, end

REPEAT_COUNT = 40
SECONDS = 15

def workout():
	print('Exercicio iniciado')
	start()

	for number in range(REPEAT_COUNT):
		print(number+1)

		sleep(SECONDS)
		switch()

	end()

if __name__ == '__main__':
	workout()
